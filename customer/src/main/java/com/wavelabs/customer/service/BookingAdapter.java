package com.wavelabs.customer.service;



import java.util.HashMap;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.wavelabs.customer.model.Booking;
public class BookingAdapter 
{
	 RestTemplate restTemplate = new RestTemplate();
	  @SuppressWarnings("rawtypes")
	public ResponseEntity<HashMap> getBookingDertailsById(Integer id){
		  String url="http://localhost:5002/booking/get?id=";
			ResponseEntity<HashMap> exchange =restTemplate.exchange(url+id, HttpMethod.GET,null,HashMap.class);
		return exchange;
		  
	  }
	  public void deleteBookingDertailsById(Integer id){
			  String url="http://localhost:5002/booking/delete?id=";
			  restTemplate.delete(url+id);
			
			  
		  }
		public Booking postBookingDetails(Booking booking){
			  String url="http://localhost:5002/booking/post";
			   
				Booking exchange =restTemplate.postForObject(url, booking, Booking.class);
				System.out.println(exchange);
			return exchange;
			  
		  }
		public Booking updateBookingDertailsById(Booking booking){
			  String url="http://localhost:5002/booking/update";
			  Booking exchange =restTemplate.postForObject(url, booking, Booking.class);			
			  System.out.println(exchange);
			return exchange;
			  
		  }
}	  

